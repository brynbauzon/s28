// Retrieve all posts following
// the REST API (retrieve, /posts, GET)
fetch('https://jsonplaceholder.typicode.com/posts')
// Use the "json" method from the "Response" object to convert data
// into JSON format to be used in our application
.then(response => response.json())
// Print the converted JSON value from the "fetch" request
.then(data => console.log(data));


// POST
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: "POST",
	headers:{
		'Content-Type': 'application/json'
	},
	//Sets the content/body data of the "Request" object to be sent to the backet
	//JSON.stringify converts the obeject data into a stringified JSON
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World',
		userId: 1
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data));


//Updating a post using PUT method - update all data from the user
//Updates a specific post following the Rest API (update, /post/:id, PUT)
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello Again',
		userId: 1
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data));


//Using Patch - update single portion of data/info of the  user
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers:{
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Correct Title'
	})
})
.then((response)=> response.json())
.then((data)=> console.log(data))


//Delete 
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE',
	headers:{
		'Content-Type': 'application/json'
	},
})
.then((response)=> response.json())
.then((data)=> console.log(data))

//Retrieve a single record
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then(response => response.json())
.then(data => console.log(data));

//CRUD
//C - CREATE (POST)
//R - READ (GET)
//U - UPDATE(PUT/PATCH)
//D - DELETE/DESTROY (DELETE)

//Server
// /users && GET

// /users/:id && GET

// /users && POST
